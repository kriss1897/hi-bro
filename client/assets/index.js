var app = function(){
    var SERVER_ADDRESS = '/server';
    var state = {
        auth: false, //user is not authenticated
        history: [],
        error: [],
        alerts: []
    }

    this.init = function(){
        initialize();
    }

    this.login = function(email,password){
        
        attemptLogin(email,password);
    }

    this.logout = function(){
        $.post(SERVER_ADDRESS,{
            type:'logout'
        },location.reload());
    }

    this.register = function(name,email,password){
        $.post(SERVER_ADDRESS,{
            type:'register',
            email:email,
            password:password,
            name:name
        },function(response){
            response = JSON.parse(response);
            if(response.status == 'success'){
                initialize();
            }else{
                alert(response.message);
            }
        });
    }

    this.sayHi = function(){
        $.post(SERVER_ADDRESS,{
            type:'sayHi'
        },function(response){
            response = JSON.parse(response);
            if(response.status == 'success'){
                initialize();
            }else{
                alert(response.message);
            }
        });
    }

    var initialize = function(){
        $.post(SERVER_ADDRESS,{
            type:'authStatus'
        },authStatus)
    }

    var authStatus = function(result){
        result = JSON.parse(result);
        state.auth = result.status;
        if(!result.status){
            $('#auth').attr('hidden',false);
            $('#app').attr('hidden',true);
        }else{
            $('#app').attr('hidden',false);
            $('#auth').attr('hidden',true);
            fetch();
        }   
    }
    
    var attemptLogin = function(email,password){
        $.post(SERVER_ADDRESS,{
            type:'login',
            email:email,
            password: password
        },function(response){
            result = JSON.parse(response);
            console.log(result);
            if(result.status == 'success'){
                initialize();
            }else{
                alert(result.message);
            }
            
        })
    }

    var fetch = function(element){
        $.post(SERVER_ADDRESS,{
            type:'fetch'
        },function(response){            
            response = JSON.parse(response);
            if(response.status == 'success'){
                render(response.data)
            }else{
                alert(response.message);
            }
        });
    }

    var render = function(messages){
        element = $("tbody#messages");
        element.empty();
        messages.map(function(message){
            code = "<tr> \
                        <td>"+message.id+"</td> \
                        <td>"+message.name+"</td> \
                        <td>"+message.time+"</td> \
                    </tr>";
            element.append(code);
        })
    }
}