<?php 
    namespace main;
    include_once 'config.php';
    use Exception;
    class API {
        private const REGISTER_QUERY = "INSERT INTO `user`(`name`, `email`, `password`, `token`) VALUES (?,?,?,?)";
        private const FETCH_QUERY = "SELECT messages.*,`user`.`name` FROM `messages`, `user` where `messages`.sender_id = `user`.id order by `time`";
        private const LOGIN_QUERY = "SELECT `password`,`token` from `user` where `user`.`email` = ?";

        private $connection;
        private $token;

        public function __construct(){
            session_start();
            $this->connection = mysqli_connect(HOST,USER,PASSWORD,DATABASE);
        }

        private function verify(){
            $token = isset($_SESSION['api-token'])?$_SESSION['api-token']:null;
            if($stmt = $this->connection->prepare('SELECT `id` from `user` where `token` = ? LIMIT 1')){
                $stmt->bind_param('s',$token);
                $stmt->bind_result($id);
                $stmt->execute();
                $stmt->fetch();
                if(isset($id))
                    $this->token = $token;
                else
                    return $this->response("failed","Invalid Token");
            }
        }

        private function checkUser($email){
            $count = 0;
            $stmt = $this->connection->prepare('SELECT count(*) from `user` where `email` = ?');
            $stmt->bind_param('s',$email);
            $stmt->bind_result($count);
            $stmt->execute();
            $stmt->fetch();
            return $count == 0;
        }
        
        public function register($name,$email,$password){
            if(isset($_SESSION['api-token'])){
                return $this->response("failed",'Already Logged In');
            }
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                if(!$this->checkUser($email)){
                    return $this->response("failed","User Already Exists");
                }
                $hashed_password = password_hash($password,PASSWORD_BCRYPT);
                $token = uniqid(null,true);
                if($stmt = $this->connection->prepare(self::REGISTER_QUERY)){
                    $stmt->bind_param('ssss',$name,$email,$hashed_password,$token);
                    if($stmt->execute()){
                        $_SESSION['api-token'] = $token;
                        return $this->response("success","User Registered");
                    }else{
                        return $this->response("failed","Query Failed. Please try again");
                    }
                }else{
                    return $this->response("failed","Query Failed. Please try again");
                }
            }
        }

        public function login($email,$password){
            if(isset($_SESSION['api-token'])){
                return $this->response("failed",'Already Logged In');
            }
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                $stmt = $this->connection->prepare(self::LOGIN_QUERY);
                $stmt->bind_param('s',$email);
                $stmt->bind_result($db_password,$token);
                $stmt->execute();
                $stmt->fetch();
                $stmt->close();
                if(password_verify($password,$db_password)){
                    $_SESSION['api-token'] = $token;
                    $this->token = $token;
                    return $this->response("success","User Logged In");
                }else{
                    return $this->response("failed",'Invalid Credentials');
                }
            }
        }

        public function fetch(){
            $this->verify();
            if($stmt = $this->connection->prepare(self::FETCH_QUERY)){
                $stmt->execute();
                $result = $stmt->get_result();
                $stmt->close();
                return $this->response("success","Messages Fetched",$result->fetch_all(MYSQLI_ASSOC));
            }else{
                $this->response("failed","Exception. Try Again.");
            }
        }

        public function sayHi(){
            $this->verify();
            if($stmt = $this->connection->prepare('SELECT `id` from `user` where `token` = ? limit 1')){
                $stmt->bind_param('s',$this->token);
                $stmt->bind_result($id);
                $stmt->execute();
                $stmt->fetch();
                $stmt->close();
            }else{
                return $this->response("failed",'Error. Try Again');
            }
            if($stmt = $this->connection->prepare("INSERT INTO `messages`(`sender_id`) VALUES (?)")){
                $stmt->bind_param('i',$id);
                if(!$stmt->execute())
                    return $this->response("failed","Sending Failed. Try Again");
                $stmt->close();
                return $this->response("success","Message Sent");
            }
        }

        public function authStatus(){
            return json_encode(array(
                    "status"=>isset($_SESSION['api-token']),
                ));
        }

        public function logout(){
            session_destroy();
            return $this->response("success","Logged Out");
        }

        private function response($status,$message, $data = null){
            return json_encode(array(
                "status"    => $status,
                "message"   => $message,
                "data"      => $data
            ));
        }
    }
?>