<?php 
    namespace main;
    include 'api.php';
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $api = new API();
        $type = $_POST['type'];
        try{
            switch($type){
                case 'authStatus':
                    $response = $api->authStatus();
                    break;
    
                case 'login':
                    $email = $_POST['email'];
                    $password = $_POST['password'];
                    $response = $api->login($email,$password);
                    break;
    
                case 'register':
                    $email = $_POST['email'];
                    $password = $_POST['password'];
                    $name = $_POST['name'];
                    $response = $api->register($name,$email,$password);
                    break;
    
                case 'fetch':
                    $response =$api->fetch();
                    break;
    
                case 'sayHi':
                    $response = $api->sayHi();
                    break;
                
                case 'logout':
                    $response = $api->logout();
                    break;
            }
            echo $response;
        }catch(Exception $e){
            echo json_encode(array(
                "status"=>"failed",
                "message"=>$e->getMessage()
            ));
        }
        
    }else{
        header('Location:client');
    }
?>