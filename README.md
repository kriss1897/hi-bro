# HiBro

## Requirements
- MySQL 5.7
- PHP 7.1

## Setup
- Create a MySQL database from ```server/database.sql```
- Update MySQL details in ```server/config.php```
- Run Project with ```php -S localhost:8000```
